{ pkgs ? import ./pinned-nixpkgs.nix {} }:

let
  python = pkgs.python37Packages;

  pythonPkgs = with python; [
    fbs
    qtawesome
    pyqt5
  ];

  devPkgs = with python; [
    python-language-server
  ];

  # None of these packages are installed
  # globally.
  systemPkgs = with pkgs; [
    gcc
    fpm
	qt5.full
  ];
in
pkgs.mkShell rec {
  name = "py-env";
  buildInputs = systemPkgs ++ pythonPkgs ++ devPkgs;
  shellHook = ''
    [ -d pip_packages ] || mkdir pip_packages

    # extra packages can be installed here
    unset SOURCE_DATE_EPOCH
    export PIP_PREFIX="$(pwd)/pip_packages"
    python_path=(
      "$PIP_PREFIX/lib/python3.7/site-packages"
      "$PYTHONPATH"
    )
    # use double single quotes to escape bash quoting
    IFS=: eval 'python_path="''${python_path[*]}"'
    export PYTHONPATH="$python_path"

    # PATH
    export PATH="$(pwd)/pip_packages/bin":$PATH
  '';
}
