{ pkgs ? import ./pinned-nixpkgs.nix {} }:

let
  python = pkgs.python37Packages;

  pythonPkgs = with python; [
    qtawesome
    pyqt5
  ];

  systemPkgs = with pkgs; [
    gcc
    fpm
	qt5.full
  ];
in
  python.buildPythonPackage rec {
    pname = "yukari";
    version = "0.0.1";

    src = ./.;

    buildInputs = pythonPkgs ++ systemPkgs;
  }
