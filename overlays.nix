self: super:

let
  py-override = {
    packageOverrides = python-self: python-super: {
      altgraph = python-super.buildPythonPackage rec {
        pname = "altgraph";
        version = "0.16.1";

        doCheck = false;

        src = python-super.fetchPypi {
          inherit pname version;
          sha256 = "ddf5320017147ba7b810198e0b6619bd7b5563aa034da388cea8546b877f9b0c";
        };
      };

      pyinstaller = python-super.buildPythonPackage rec {
        pname = "PyInstaller";
        version = "3.5";

        doCheck = false;

        propagatedBuildInputs = with python-self; [
          altgraph
        ];

        src = python-super.fetchPypi {
          inherit pname version;
          sha256 = "ee7504022d1332a3324250faf2135ea56ac71fdb6309cff8cd235de26b1d0a96";
        };
      };

      fbs = python-super.buildPythonPackage rec {
        pname = "fbs";
        version = "0.8.3";

        doCheck = false;

        propagatedBuildInputs = with python-self; [ pyinstaller ];

        src = super.fetchFromGitHub {
          owner = "mherrmann";
          repo = "fbs";
          rev = "f1d84da8d017358db08edfd21c03d2990e39b37e";
          sha256 = "1n4fpzl1n6f341rkjrajxrm52azikly7z2a06w6yrzvlcm9jvsm0";
        };
      };
    };
  };
in
{
  python2 = super.python2.override py-override;
  python3 = super.python3.override py-override;
  python37 = super.python37.override py-override;
}
